#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>

/*
  ==============================================================================
  Error codes.
  ==============================================================================
*/
/* We couldn't list directory contents */
#define DIRFAILED 1
/* We couldn't open a file for writing */
#define FILEFAILED 2
/* We couldn't find the $USER env var */
#define NOUSERENV 3
/* We couldn't find the $HOSTNAME env var */
#define NOHOSTENV 4

/*
  ==============================================================================
  Basic configuration
  ==============================================================================
*/
#define TICKET_FOLDER "./tickets"
/* #define OVERRIDE_USER "johnny" */
/* #define OVERRIDE_DOMAIN "yourhost-or-emaildomain.local" */
/* #define DEBUG_MODE */

/*
  ==============================================================================
  Program Code
  ==============================================================================
*/

/*
  This var is for the max ticket no we've seen, so essentially setting it sets a
  bottom limit for ticket numbers.
*/
int max_ticket_no = 0;

/*
  read_dir(path)

  Checks the specified directory for ticket numbers, and attempts to find the
  largest one.
*/
int read_dir(
    char * path)
{
  /* set up the dirent.h-related structures */
  DIR * pwd;
  struct dirent * pwd_details;
  pwd = opendir(path);

  /* temporary ticket number field */
  int ticket_no = 0;

  /* Handle if we can't read the directory */
  if(pwd == NULL)
    {
      fprintf(
          stderr,
          "Error: couldn't list contents of %s! Check that the dir exists.\n",
          path);
      return DIRFAILED;
    }

  /* 
     Try and find the maximum ticket number in this directory
     
     DANGER: Right now this is built for directories that have nothing but
             numbered files in them, anything else will cause undefined behavior
  */
  while(pwd_details = readdir(pwd))
    {
      ticket_no = atoi (
          pwd_details -> d_name);
#ifdef DEBUG_MODE
      /*
        Enabling debug mode will allow us to see exactly what numbers atoi came
        up with if we have a corrupted directory we need to sniff later down the
        line.
      */
      printf(
          "%d ticket no found\n",
          ticket_no);
#endif /* DEBUG_MODE */
      if(ticket_no > max_ticket_no)
        {
          max_ticket_no = ticket_no;
        }
    }
  /* Then we clean up and leave. Job done. */
  closedir(pwd);
  return 0;
}

/*
  make_ticket(path)
  
  Makes a new ticket at the specified path, creating a file named max_ticket+1
  and leaving it blank for later use.
*/
int make_ticket(
    char * path)
{
  FILE * newticket   = NULL;
  char   filename[16];
  char * uname       = NULL;
  char * hostname    = NULL;

  /* Build the full file path with all needed info and ticket no. */
  sprintf(
      filename,
      "%s/%d",
      path,
      ++max_ticket_no);

  /* open the file for writing, check and make sure we have a file ready */
  newticket = fopen(
      filename,
      "w");
  if(newticket == NULL)
    {
      fprintf(
          stderr,
          "Error: couldn't write to new file %s!\n",
          filename);
      return FILEFAILED;
    }

  /* 
     Now we get the user and host settings either from the environment or
     overrides. We then make sure those are defined so we can set a uname.
  */
#ifdef OVERRIDE_USER
  uname    = OVERRIDE_USER;
#else
  uname    = getenv("USER");
#endif
#ifdef OVERRIDE_HOSTNAME
  hostname = OVERRIDE_HOSTNAME;
#else
  hostname = getenv("HOSTNAME");
#endif
  if(uname == NULL)
    {
      fprintf(
          stderr,
          "Error: couldn't find the $USER env variable!\n");
      return NOUSERENV;
    }
  if(hostname == NULL)
    {
      fprintf(
          stderr,
          "Error: couldn't find the $HOSTNAME env variable!\n");
      return NOHOSTENV;
    }

  /* Now we print all the details we have now to a file. */
  fprintf(
      newticket,
      "ticket:%d\n",
      max_ticket_no);
  fprintf(
      newticket,
      "responsible:%s@%s\n",
      uname,
      hostname);
  fprintf(
      newticket,
      "status:open\n");
  fprintf(
      newticket,
      "title:???\n");
  fprintf(
      newticket,
      "---------------\n");
  fprintf(
      newticket,
      "Describe your problem here.");

  /* Clean up, we're done. */
  fclose(
      newticket);
  return 0;
}

/*
  Main is, as per my (CannonContraption) personal liking very minimal, just some
  glue to make the program actually do something.
*/
int main()
{
  int status = 0;
  status = read_dir(
      TICKET_FOLDER);
  if(status > 0) return status;
  status = make_ticket(
      TICKET_FOLDER);
  return status;
}
