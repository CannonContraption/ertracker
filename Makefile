CC     = gcc
CCOPTS = -O3

default: new_ticket

new_ticket: new_ticket.c
	$(CC) new_ticket.c -o new_ticket $(CCOPTS)
